namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // ������������ StartPosition
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            textBox5.Visible = false;
            textBox6.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b = 0, c, D = 0; 

            if (double.TryParse(textBox1.Text, out a)) {


                if(double.TryParse(textBox2.Text, out b))
                {
                    if(double.TryParse(textBox3.Text, out c))
                    {
                        D = Math.Pow(b, 2) - 4 * a * c;

                        textBox4.Text = D.ToString("F2");
                    }
                    else
                    {
                        MessageBox.Show("������� �������� �������� c!", "�������",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show("������� �������� �������� b!", "�������",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("������� �������� �������� a!","�������", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (D >= 0)
            {
                textBox5.Visible = true;
                label5.Visible = true;
                textBox5.Text = (-1 * b + Math.Sqrt(D)).ToString("F2");
                
            }
            if (D > 0)
            {
                textBox6.Visible = true;
                label6.Visible = true;
                textBox6.Text = (-1 * b - Math.Sqrt(D)).ToString("F2");
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}